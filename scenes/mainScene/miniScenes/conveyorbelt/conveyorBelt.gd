extends Node


export var speed : int
export var forwards : bool

var foods : Array

onready var sprite = $Conveyor

# Called when the node enters the scene tree for the first time.
func _ready():
	var scene = load("res://scenes/mainScene/miniScenes/foods/interchangableFood.tscn")
	var scene_instance = scene.instance()
	scene_instance.set_name("scene")
	foods.push_front(scene_instance)
	add_child(scene_instance)
	if (forwards):
		sprite.rotation_degrees = 180
	else:
		speed *= -1


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	for food in foods:
		food.position.x += speed * delta
	sprite.texture.region.position.x -= speed * delta
