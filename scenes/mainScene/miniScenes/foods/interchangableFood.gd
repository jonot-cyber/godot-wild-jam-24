extends Node2D

export(String, "bread", "cheese", "cucumber") var foods

var food = {"bread": 50, "cheese": 75, "cucumber": 100}

onready var sprite = get_node("FoodSprite")

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite.texture = load("res://scenes/mainScene/assets/images/food/" + foods + ".svg")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
